# coding:utf-8

from PIL import Image, ImageDraw

def draw_image(percent):
	n = int(200 * percent)
	imbg = Image.new('RGB',(200,20), (245,245,245))
	imft = Image.new('RGB',(n,20), (0,255,0))
	imbg.paste(imft, (0,0))
	x,y = imbg.size
	draw = ImageDraw.Draw(imbg)
	draw.text((int(x * percent) , int(y * 0.3)), str("{}%".format(percent*100)), fill=(0,0,0))
	# imbg.save(r'd:\a.bmp')
	# imbg.show()
	return imbg
	 
#img = draw_image(0.6)
#img.show()
