import ui
from drawp import draw_image
import io


img = draw_image(0.4)
#img.save('usage.jpg')
v = ui.load_view('imgDemo')
iv = v['imageview1']

def pil2ui(img):
	with io.BytesIO() as bIO:
		img.save(bIO, 'PNG')
		imgOut = ui.Image.from_data(bIO.getvalue())
		del bIO
		return imgOut
iv.image = pil2ui(img)
#print(dir(iv.image)) 
v.present('sheet')
