# coding:utf-8
import requests
import xml.etree.ElementTree as ET
import ui
from dialogs import hud_alert
import pickle

class VPS():
    
    def __init__(self):
        self.api_key = 'your self key'
        self.api_hash = 'your self hash'
        self.api_url = 'your self url'
    
    def __exec_api(self,**kwargs):
        #action = kwargs['action']
        payload = {'key':self.api_key,'hash':self.api_hash}
        payload = dict(payload,**kwargs)
        r = requests.get(self.api_url,params=payload)
        return r.text
    def info(self):
        action_dic = {'action':'info','mem':'true','hdd':'true','bw':'true'}
        return self.__exec_api(**action_dic)
        """
        ipaddr (Returns a list of ipaddresses)
        hdd    (Returns the hard disk usage in Bytes) 
        mem    (Returns the memory usage in Bytes)
        bw     (Returns the bandwidth usage in Bytes)
        <ipaddr>123.123.123.123</ipaddr>
        <hdd>total,used,free,percentused</hdd> 
        <mem>total,used,free,percentused</mem>
        <bw>total,used,free,percentused</bw>
        """
        
    def status(self):
        action_dic = {'action':'status'}
        return self.__exec_api(**action_dic)

    def reboot(self):
        action_dic = {'action':'reboot'}
        return self.__exec_api(**action_dic)

    def boot(self):
        action_dic = {'action':'boot'}
        return self.__exec_api(**action_dic)
    
    def shutdown(self):
        action_dic = {'action':'shutdown'}
        return self.__exec_api(**action_dic)
# data = {}
@ui.in_background
def domain(sender):
	global data
	data = {}
	# 连接button label 等
	alt_msg = '获取数据中...请稍后'
	hud_alert(alt_msg,'success',5)
	status = "<resp>{}</resp>".format(s1().status())
	info = "<resp>{}</resp>".format(s1().info())
	statusRoot = ET.fromstring(status)
	infoRoot = ET.fromstring(info)
	# data = {}
	for statusElem in  statusRoot.iter():
	    data.setdefault(statusElem.tag, statusElem.text)
	for infoElem in infoRoot.iter():
	    data.setdefault(infoElem.tag, infoElem.text)
	#print('实时data {}'.format(data))
	with open('vps.pki', 'wb') as f:
		pickle.dump(data, f)
	hdd = data['hdd']
	hdd_usage = hdd.split(',')[3]
	bw = data['bw']
	bw_usage = bw.split(',')[3]
	
	lb_hostname = sender.superview['label2']
	lb_hostname.text = data['hostname']
	lb_ipaddr = sender.superview['label4']
	lb_ipaddr.text = data['ipaddress']
	lb_vmstat = sender.superview['label6']
	lb_vmstat.text = data['vmstat']
	lb_bwusage = sender.superview['label8']
	lb_bwusage.text = '{} %'.format(bw_usage)
	lb_hddusage = sender.superview['label10']
	lb_hddusage.text = '{} %'.format(hdd_usage)
	done_msg = '刷新完成!'
	hud_alert(done_msg,'success',2)

@ui.in_background
def shutdown(sender):
	hud_alert('关机中.. 请稍后', 'success', 2)
	msg = s1().shutdown()
	hud_alert(msg, 'success',2)
	
@ui.in_background
def boot(sender):
	hud_alert('正在开机..稍后刷新状态', 'success', 3)
	msg = s1().boot()
	hud_alert(msg, 'success',2)

@ui.in_background
def dostop(sender):
	s.close()

s = ui.load_view('solusVM')
s1 = VPS
with open('vps.pki', 'rb') as f:
	data = pickle.load(f)
	#print('本地缓存的data {}'.format(data))

s['label2'].text = data['hostname']
s['label4'].text = data['ipaddress']
s.present(hide_title_bar=True)


